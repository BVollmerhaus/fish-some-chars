# Test cases for highlighting the length of selected strings.
#
# Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
# License: MIT

@test "Length is highlighted for empty string" (
    mock commandline -C 0 "printf 1"
    mock commandline \* 0 "printf '\"\"'"
    printf (_fish_some_chars_prompt)
) = (
    printf "%s0%s ch" (set_color -o blue) (set_color normal)
)

@test "Length is highlighted if cursor within string" (
    mock commandline -C 0 "printf 10"
    mock commandline \* 0 "printf '\"Hello, World\"'"
    _fish_some_chars_prompt
) = (
    printf "%s12%s ch" (set_color -o blue) (set_color normal)
)

@test "Length is highlighted if cursor at first character" (
    mock commandline -C 0 "printf 1"
    mock commandline \* 0 "printf '\"Hello, World\"'"
    _fish_some_chars_prompt
) = (
    printf "%s12%s ch" (set_color -o blue) (set_color normal)
)

@test "Length is highlighted if cursor at last character" (
    mock commandline -C 0 "printf 13"
    mock commandline \* 0 "printf '\"Hello, World\"'"
    _fish_some_chars_prompt
) = (
    printf "%s12%s ch" (set_color -o blue) (set_color normal)
)

@test "Length is not highlighted if cursor before first character" (
    mock commandline -C 0 "printf 0"
    mock commandline \* 0 "printf '\"Hello, World\"'"
    _fish_some_chars_prompt
) = "12 ch"

@test "Length is not highlighted if cursor after last character" (
    mock commandline -C 0 "printf 14"
    mock commandline \* 0 "printf '\"Hello, World\"'"
    _fish_some_chars_prompt
) = "12 ch"

@test "Correct length is highlighted with multiple strings" (
    mock commandline -C 0 "printf 12"
    mock commandline \* 0 "printf '\"Hello, \" \"World\"'"
    _fish_some_chars_prompt
) = (
    printf "7 / %s5%s ch" (set_color -o blue) (set_color normal)
)
