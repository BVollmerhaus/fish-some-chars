# Test cases for argument parsing.
#
# Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
# License: MIT

@test "'-s' argument modifies printed suffix" (
    mock commandline -C 0 "printf 0"
    mock commandline \* 0 "printf '\"\"'"
    _fish_some_chars_prompt -s "bananas"
) = "0 bananas"

@test "'--suffix' argument modifies printed suffix" (
    mock commandline -C 0 "printf 0"
    mock commandline \* 0 "printf '\"\"'"
    _fish_some_chars_prompt --suffix "bananas"
) = "0 bananas"

# Only run set_color-based tests if in a tty (i.e. not in CI)
if isatty
    @test "'-c' argument modifies highlight color" (
        mock commandline -C 0 "printf 1"
        mock commandline \* 0 "printf '\"\"'"
        _fish_some_chars_prompt -c brcyan
    ) = (
        printf "%s0%s ch" (set_color -o brcyan) (set_color normal)
    )

    @test "'--color' argument modifies highlight color" (
        mock commandline -C 0 "printf 1"
        mock commandline \* 0 "printf '\"\"'"
        _fish_some_chars_prompt --color brcyan
    ) = (
        printf "%s0%s ch" (set_color -o brcyan) (set_color normal)
    )

    @test "Invalid '-c' argument exits with non-zero status" (
        mock commandline -C 0 "printf 1"
        mock commandline \* 0 "printf '\"\"'"
        _fish_some_chars_prompt -c invalid 2>/dev/null
    ) $status -eq 1
end
