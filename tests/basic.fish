# Test cases for basic display of string lengths.
#
# Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
# License: MIT

@test "Prints nothing if no string in command" (
    mock commandline -C 0 "printf 0"
    mock commandline \* 0 "printf ''"
    _fish_some_chars_prompt
) -z

@test "Length is 0 if string is empty" (
    mock commandline -C 0 "printf 0"
    mock commandline \* 0 "printf 'echo \"\"'"
    _fish_some_chars_prompt
) = "0 ch"

@test "Length is correct for double-quoted string" (
    mock commandline -C 0 "printf 0"
    mock commandline \* 0 "printf 'echo \"In double quotes\"'"
    _fish_some_chars_prompt
) = "16 ch"

@test "Length is correct for single-quoted string" (
    mock commandline -C 0 "printf 0"
    mock commandline \* 0 "printf \"echo 'In single quotes'\""
    _fish_some_chars_prompt
) = "16 ch"

@test "Lengths are correct for multiple strings" (
    mock commandline -C 0 "printf 0"
    mock commandline \* 0 "printf 'echo \"Hello, \"; cmd \"World\" \"!\"'"
    _fish_some_chars_prompt
) = "7 / 5 / 1 ch"
