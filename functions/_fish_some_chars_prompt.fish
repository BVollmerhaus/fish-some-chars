# Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
# License: MIT

function _fish_some_chars_prompt -d "Print the length of strings in the current command line"
    set -l string_regex '"(?:\\\.|[^"])*"|\'(?:\\\.|[^\'])*\''

    argparse -n _fish_some_chars_prompt 's/suffix=' 'c/color=!__fish_some_chars_validate_color' -- $argv
    or return
    set -q _flag_suffix; or set -l _flag_suffix 'ch'
    set -q _flag_color; or set -l _flag_color 'blue'

    set -l cursor_pos (commandline -C)
    set -l string_ranges (string match -a -r -n $string_regex (commandline))

    for range in $string_ranges
        set -l start_len (string split ' ' $range)
        set -l start $start_len[1]
        set -l length (math $start_len[2] - 2)

        if test $cursor_pos -ge $start
            and test $cursor_pos -le (math $start + $length)
            printf "%s$length%s" (set_color -o $_flag_color) (set_color normal)
        else
            printf $length
        end

        if test $range != $string_ranges[-1]
            printf ' / '
        end
    end

    if test -n "$string_ranges"
        printf " $_flag_suffix"
    end
end
