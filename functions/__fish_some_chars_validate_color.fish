# Validate a color argument by checking whether it's accepted by set_color.
#
# See: https://fishshell.com/docs/current/cmds/set_color.html
#
# Author: Benedikt Vollmerhaus <benedikt@vollmerhaus.org>
# License: MIT

function __fish_some_chars_validate_color --no-scope-shadowing
    set_color $_flag_value >/dev/null 2>&1
    if test $status -ne 0
        printf "%s: Invalid value '%s' for -c/--color; must be supported by %sset_color%s." \
          $_argparse_cmd $_flag_value (set_color blue) (set_color normal)
        return 1
    end

    return 0
end
