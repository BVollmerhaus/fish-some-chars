![](banner.png)

# fish-some-chars

A [fish](https://fishshell.com/) plugin that displays the length of
strings in your current command.


## Installation

**Using [Fisher](https://github.com/jorgebucaran/fisher)**
```
fisher add gitlab.com/BVollmerhaus/fish-some-chars
```

**Using [Oh My Fish](https://github.com/oh-my-fish/oh-my-fish)**
```
omf install gitlab.com/BVollmerhaus/fish-some-chars
```


## Setup

> This plugin is intended to display on the prompt, so you'll have to
  add it to your left or right prompt function manually.  
  If you haven't customized the prompt yet, you may need to create the
  respective function first to override fish's default.

1.  Add a call to `_fish_some_chars_prompt` in either
    [prompt function](https://fishshell.com/docs/current/index.html#programmable-prompt),
    depending on where you want the lengths to be shown.

    > **Example (right prompt):**
    > ~/.config/fish/functions/fish_right_prompt.fish
    > ```
    > function fish_right_prompt -d "Write out the right prompt"
    >     _fish_some_chars_prompt
    > end
    > ```

2.  Create a [key binding](https://fishshell.com/docs/current/cmds/bind.html)
    for `commandline -f repaint` (for example in `config.fish`) to display or
    update the plugin's prompt.

    > **Example (bound to <kbd>Alt</kbd>+<kbd>C</kbd>):**
    > ```
    > bind \ec 'commandline -f repaint'
    > ```


## Usage

![](example.svg)

*   Press the configured key bind when there is at least one string (single
    or double quoted) in the current command.
*   Each string's length will be shown on the prompt, with the length of a
    string under your cursor being highlighted.


## Customization

The following arguments may be passed when calling `_fish_some_chars_prompt`
to customize its output:

| Short / Long | Default | Description |
|--------------|---------|-------------|
| `-s` / `--suffix <string>` | "ch" | Specify a suffix to add after all printed lengths
| `-c` / `--color <color>`   | blue | Specify a color to use for highlighting (must be [supported by set_color](https://fishshell.com/docs/current/cmds/set_color.html#description))
